#pragma once

#include <unordered_map>
#include "ShaderInterface.h"

struct Buffer {
	void* pData;

	Buffer() 
		: pData(NULL) {}

	~Buffer(void)  {
		if (pData) {
			delete [] pData;
		}
	}
};

struct GraphicsContext {
	GraphicsContext(void)
		: arrayBuffer(NULL)
		, elementBuffer(NULL)
		, positionAttribute(NULL)
		, colorAttribute(NULL)
		, normalAttribute(NULL)
		, currentShader(NULL) {}

	~GraphicsContext(void) {
		std::unordered_map<unsigned int, Buffer*>::iterator iter = buffers.begin();
		while (iter != buffers.end()) {
			delete iter->second;
			iter++;
		}
	}

	Buffer *arrayBuffer;
	Buffer *elementBuffer;
	std::unordered_map<unsigned int, Buffer*> buffers;

	// These values point to the beginning location in the correct buffer of the positions, colors, and normals of the data.  
	// Basically a hugely simplified version of a vertex attribute pointer implementation...since this project is pretty small 
	// and creating amazing programmable shaders isn't my main objective, I'm going to avoid a general approach for now.
	glm::vec4 *positionAttribute;
	glm::vec4 *colorAttribute;
	glm::vec4 *normalAttribute;

	ShaderInterface *currentShader;
};