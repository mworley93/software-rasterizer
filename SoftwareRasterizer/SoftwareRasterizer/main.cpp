#include "SDL.h"
#include <stdio.h>
#include "Color.h"
#include "Rasterizer.h"
#include <cmath>
#include <glm\glm.hpp>
#include "Camera.h"
#include "Mesh.h"
#include "PhongShader.h"
#include "RenderSettings.h"

// Function prototypes.
void InitRenderSettings(SDL_Renderer*);
void InitRasterizer(Rasterizer*);
void CalculateFPS(unsigned int&);
void EventLoop(SDL_Renderer*);
void HandleUserEvents(const SDL_Event&);

// Global variables & constants.
bool g_Running;
const unsigned int WIDTH = 640;
const unsigned int HEIGHT = 480;
const glm::vec3 LIGHT_POSITION(0.0f, 2.0f, 1.5f);

Camera camera;
unsigned int vertexBuffer;

int main(int argc, char* argv[]) {
	// Initialize SDL2
    SDL_Init(SDL_INIT_VIDEO);

	// Create the rendering context.
	SDL_Window *sdlWindow;
	SDL_Renderer *sdlRenderer;
	SDL_CreateWindowAndRenderer(WIDTH, HEIGHT, SDL_WINDOW_OPENGL, &sdlWindow, &sdlRenderer);

	// Check that the window was successfully made
    if (sdlWindow == NULL) {
        printf("Could not create window: %s\n", SDL_GetError());
        return 1;
    }

	InitRenderSettings(sdlRenderer);
	EventLoop(sdlRenderer);

    // Clean up
    SDL_DestroyWindow(sdlWindow);
    SDL_Quit();
    return 0;
}

void InitRenderSettings(SDL_Renderer* renderer) {
	// Clear the screen to black.
	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
	SDL_RenderClear(renderer);
	SDL_RenderPresent(renderer);

	SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "linear");  // make the scaled rendering look smoother.
	SDL_RenderSetLogicalSize(renderer, WIDTH, HEIGHT);
	camera.lookAt(glm::vec3(0, 0, 5), glm::vec3(0, 0, -1), glm::vec3(0, 1, 0));
	g_Running = true;
}

void InitRasterizer(Rasterizer *rasterizer) {
	rasterizer->setClearColor(Color(0.15f, 0.18f, 0.35f, 1.0f));  // Sets background color to dark blue.

	// Enable depth testing.
	rasterizer->enableDepthTesting(true);

	// Set face culling properties.
	rasterizer->enableFaceCulling(true);
	rasterizer->cullFace(BACK);
	rasterizer->setFrontFace(COUNTER_CLOCKWISE);
}

void EventLoop(SDL_Renderer* renderer) {
	SDL_Texture* sdlTexture = SDL_CreateTexture(renderer,
                               SDL_PIXELFORMAT_ARGB8888,
                               SDL_TEXTUREACCESS_STREAMING,
                               WIDTH, HEIGHT);

	Rasterizer *rasterizer = new Rasterizer(WIDTH, HEIGHT);
	InitRasterizer(rasterizer);
	
	// Create a basic shader.
	PhongShader *shader = new PhongShader();
	rasterizer->useShader(shader);
	shader->setLightPosition(LIGHT_POSITION);

	// Initialize the 3d model.
	Mesh model;
	model.setRasterizer(rasterizer);
	model.loadObj("..\\..\\models\\suzanne.obj");

	unsigned int lastTicks = 0;

	while(g_Running) {
		// handle events
		SDL_Event event;
		while(SDL_PollEvent(&event))
			HandleUserEvents(event);

		// Clear the framebuffer and render the next frame.
		rasterizer->clear();
		glm::mat4 viewMat = camera.getViewMatrix();
		shader->setModelViewMatrix(viewMat);
		shader->setViewToClipMatrix(camera.getPerspectiveProjection(45, (float)WIDTH/(float)HEIGHT, 0.5f, 20));
		shader->setNormalMatrix(glm::transpose(glm::inverse(viewMat)));
		model.render();
		SDL_UpdateTexture(sdlTexture, NULL, rasterizer->getFramebuffer(), WIDTH * sizeof (Uint32));
		SDL_RenderClear(renderer);
		SDL_RenderCopy(renderer, sdlTexture, NULL, NULL);
		SDL_RenderPresent(renderer);
		
		CalculateFPS(lastTicks);
	}
	rasterizer->deleteBuffer(vertexBuffer);
	delete rasterizer;
	model.setRasterizer(NULL);
}


void CalculateFPS(unsigned int &lastTicks) {
	unsigned int ticks = SDL_GetTicks();
	unsigned int ticksDiff = ticks - lastTicks;
	if(ticksDiff == 0)
		return;
	float time = ticksDiff / 1000.0f;
	unsigned int fps = 1000 / ticksDiff;
	printf("Frames per second: %u    \r", fps);
	lastTicks = ticks;
}

static void HandleKeyEvent(const SDL_Event &event) {
	switch(event.key.keysym.sym) {
		default:
			break;
		case SDLK_ESCAPE:
			g_Running = false;
			break;
		case SDLK_UP:
			camera.rotate(-camera.getSideVector(), 3);
			break;
		case SDLK_DOWN:
			camera.rotate(-camera.getSideVector(), -3);
			break;
		case SDLK_RIGHT:
			camera.rotate(camera.getOrthoUpVector(), 3);
			break;
		case SDLK_LEFT:
			camera.rotate(camera.getOrthoUpVector(), -3);
			break;
		case SDLK_z:
			camera.rotate(camera.getLookVector(), -3);
			break;
		case SDLK_x:
			camera.rotate(camera.getLookVector(), 3);
			break;
		case SDLK_w:
			camera.move(Camera::UP, 0.1f);
			break;
		case SDLK_s:
			camera.move(Camera::DOWN, 0.1f);
			break;
		case SDLK_d:
			camera.move(Camera::RIGHT, 0.1f);
			break;
		case SDLK_a:
			camera.move(Camera::LEFT, 0.1f);
			break;
		case SDLK_q:
			camera.move(Camera::BACKWARD, 0.1f);
			break;
		case SDLK_e:
			camera.move(Camera::FORWARD, 0.1f);
			break;
	}
}

static void HandleUserEvents(const SDL_Event &event) {
	switch(event.type) {
		default:
			break;
		case SDL_QUIT:
			g_Running = false;
			break;
		case SDL_KEYDOWN:
			HandleKeyEvent(event);
			break;
	}
}