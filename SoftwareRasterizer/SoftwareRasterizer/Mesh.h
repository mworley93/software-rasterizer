#pragma once
#include "Rasterizer.h"
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include "RenderSettings.h"

/*
* This class represents a mesh that is loaded in from an external file.  Currently only loading from the .obj file format is supported.  It manages 
* all of the buffer allocation for the object as well as the rendering.
*/
class Mesh
{
public:
	Mesh(void);
	~Mesh(void);

	/*
	* Loads a model from a .obj file.  
	* Assistance from: http://en.wikibooks.org/wiki/OpenGL_Programming/Modern_OpenGL_Tutorial_Load_OBJ
	* and http://www.opengl-tutorial.org/beginners-tutorials/tutorial-7-model-loading/#Example_OBJ_file.
	*/
	void loadObj(const char filename[]);

	void setRasterizer(Rasterizer *rasterizer);

	/*
	* Renders the object.
	*/
	void render();

private:
	Rasterizer *m_Rasterizer;
	unsigned int m_VertexBuffer;
	unsigned int m_ElementBuffer;

	std::vector<glm::vec4> m_Vertices;
	std::vector<glm::vec4> m_Normals;
	std::vector<glm::vec4> m_UVs;
	std::vector<short> m_VertexIndices;
	std::vector<short> m_NormalIndices;
	std::vector<short> m_uvIndices;

	std::vector<glm::vec4> m_Data;
	std::vector<short> m_Indices;
	int m_NumPoints;

	void initializeBuffers();
	void clearData();
	void resolveIndices();
};

