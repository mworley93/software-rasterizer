#include "Mesh.h"


Mesh::Mesh(void)
	: m_Rasterizer(NULL) {
}


Mesh::~Mesh(void) {
	if (m_Rasterizer) {
		m_Rasterizer->deleteBuffer(m_VertexBuffer);
		m_Rasterizer->deleteBuffer(m_ElementBuffer);
	}
}

void Mesh::setRasterizer(Rasterizer *rasterizer) {
	m_Rasterizer = rasterizer;
}

void Mesh::loadObj(const char filename[]) {
	clearData();
	std::ifstream inFile(filename, std::ios::in);
	if (!inFile) {
		// Could not open file.
		printf("Could not open file.");
		return;
	}
	std::string line;
	while (getline(inFile, line)) {
		int start = line.find_first_not_of(" ");
		if (start >= 0) {
			// Load vertices.
			if (line.substr(start, 2) == "v "){
				std::istringstream istream(line.substr(start + 2));
				glm::vec4 vertex; 
				istream >> vertex.x; istream >> vertex.y; istream >> vertex.z; vertex.w = 1.0f;
				m_Vertices.push_back(vertex);
				m_UVs.push_back(glm::vec4(0.55f, 0.4f, 0.3f, 1.0f));
			}
			// Load normals.
			else if (line.substr(start, 2) == "vn"){
				std::istringstream istream(line.substr(start + 2));
				glm::vec4 normal; 
				istream >> normal.x; istream >> normal.y; istream >> normal.z; normal.w = 1.0f;
				m_Normals.push_back(normal);
			}
			// Load face indicies.
			else if (line.substr(start, 2) == "f ") {
				int vertexIndex[3], normalIndex[3], uvIndex[3];
				std::string face = line.substr(start + 2);
				int matches = sscanf(face.c_str(), "%d/%d/%d %d/%d/%d %d/%d/%d\n", &vertexIndex[0], &uvIndex[0], &normalIndex[0], &vertexIndex[1], 
					&uvIndex[1], &normalIndex[1], &vertexIndex[2], &uvIndex[2], &normalIndex[2]);
				if (matches != 9){
					printf("File can't be read; faces aren't triangulated.  Try exporting with other options\n");
					clearData();
					return;
				}

				for (int i = 0; i < 3; ++i) {
					m_VertexIndices.push_back(vertexIndex[i]-1);
					m_uvIndices.push_back(0);
					m_NormalIndices.push_back(normalIndex[i]-1);
				}
			}
		}
	}
	resolveIndices();
	initializeBuffers();
}

void Mesh::initializeBuffers() {
	m_Rasterizer->generateBuffer(m_VertexBuffer);
	m_Rasterizer->bindBuffer(ARRAY_BUFFER, m_VertexBuffer);
	m_Rasterizer->bufferData(ARRAY_BUFFER, m_Data.size(), &m_Data[0]);
	m_Rasterizer->setVertexAttributePointer(POSITIONS, 0);
	m_Rasterizer->setVertexAttributePointer(COLORS, m_NumPoints);
	m_Rasterizer->setVertexAttributePointer(NORMALS, m_NumPoints*2);
	m_Rasterizer->bindBuffer(ARRAY_BUFFER, 0);

	m_Rasterizer->generateBuffer(m_ElementBuffer);
	m_Rasterizer->bindBuffer(ELEMENT_BUFFER, m_ElementBuffer);
	m_Rasterizer->bufferData(ELEMENT_BUFFER, m_Indices.size(), &m_Indices[0]);
	m_Rasterizer->bindBuffer(ELEMENT_BUFFER, 0);
}

void Mesh::clearData() {
	m_Vertices.clear();
	m_Normals.clear();
	m_UVs.clear();
	m_VertexIndices.clear();
	m_NormalIndices.clear();
	m_Data.clear();
	m_Indices.clear();
	if (m_Rasterizer) {
		m_Rasterizer->deleteBuffer(m_VertexBuffer);
		m_Rasterizer->deleteBuffer(m_ElementBuffer);
	}
}

void Mesh::resolveIndices() {
	std::vector<glm::vec4> t_Vertices;
	std::vector<glm::vec4> t_Normals;
	std::vector<glm::vec4> t_UVs;
	for (int i = 0; i < m_VertexIndices.size(); i++) {
		bool found = false;
		for (int j = 0; j < t_Vertices.size(); j++) {
			// Check to see if this vertex has already been added to the mesh.
			if ((m_Vertices[m_VertexIndices[i]] == t_Vertices[j]) && (m_Normals[m_NormalIndices[i]] == t_Normals[j])) {
				found = true;
				m_Indices.push_back(j);
				break;
			}
		}
		// Add a new vertex if the mesh does not already contain it.
		if (!found) {
			t_Vertices.push_back(m_Vertices[m_VertexIndices[i]]);
			t_Normals.push_back(m_Normals[m_NormalIndices[i]]);
			t_UVs.push_back(m_UVs[m_uvIndices[i]]);
			m_Indices.push_back(t_Vertices.size() - 1);
		}
	}

	for (int i = 0; i < t_Vertices.size(); i++) {
		m_Data.push_back(t_Vertices[i]);
	}
	for (int i = 0; i < t_UVs.size(); i++) {
		m_Data.push_back(t_UVs[i]);
	}
	for (int i = 0; i < t_Normals.size(); i++) {
		m_Data.push_back(t_Normals[i]);
	}
	m_NumPoints = t_Vertices.size();
}

void Mesh::render() {
	m_Rasterizer->bindBuffer(ARRAY_BUFFER, m_VertexBuffer);
	m_Rasterizer->bindBuffer(ELEMENT_BUFFER, m_ElementBuffer);
	m_Rasterizer->drawElements(TRIANGLES, 0, m_Indices.size());
	m_Rasterizer->bindBuffer(ELEMENT_BUFFER, 0);
	m_Rasterizer->bindBuffer(ARRAY_BUFFER, 0);
}
