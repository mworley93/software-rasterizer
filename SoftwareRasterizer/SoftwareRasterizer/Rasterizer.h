#pragma once
#include <stdint.h>
#include <algorithm>
#include "Color.h"
#include <glm\glm.hpp>
#include <vector>
#include <algorithm>
#include <iostream>
#include "Plane.h"
#include "ClippingPlanes.h"
#include "GraphicsContext.h"
#include <time.h>
#include <unordered_map>
#include <iterator>
#include "ShaderInterface.h"
#include "Line.h"
#include "Triangle.h"
#include "RenderSettings.h"

/*
* This class is responsible for drawing given vertices to the screen.  It can render the vertices as points, lines, or triangles, specified 
* by the RenderMode given to the draw function.  It performs the clipping and rasterization stages of the graphics pipeline.  It also supports 
* very simple programmable shaders written in C++.
*/
class Rasterizer
{
public:
	/**
	* Main constructor.
	* Params:
	*	width - The width of the screen in pixels.
	*	height - The height of the screen in pixels.
	*/
	Rasterizer(const unsigned int width, const unsigned int height);

	~Rasterizer(void);

	uint32_t* getFramebuffer();

	/**
	* Sets the color that the color buffer should be cleared by every frame.
	*/
	void setClearColor(Color color);

	/**
	* Clears the framebuffer.
	*/
	void clear();

	/*
	* Creates a new buffer object.
	* handle - Modified as a side effect.  Returns the handle to the generated buffer object.
	*/
	void generateBuffer(unsigned int &handle);

	/*
	* Binds a buffer to a target.
	* target - The binding target.
	* handle - The handle to the buffer object.
	*/
	void bindBuffer(BufferType target, unsigned int handle);

	/*
	* Copies the given data to the buffer set at the binding target.
	* target - The binding target/type of buffer (see BufferType).
	* count - The number of elements that should be copied.
	* data - A pointer to the beginning of the data that should be copied to the buffer.
	*/
	void bufferData(BufferType target, unsigned int count, const void *data);

	/*
	* Deletes the given buffer.  If the buffer is currently bound, the binding reverts to 0 and then the buffer is deleted.  It name 
	* is then free to be reused.
	* Params:
	*	handle - The handle to the buffer to delete.
	*/
	void deleteBuffer(unsigned int handle);

	/*
	* Sets a pointer that indicates where in the currently bound buffer the vertex attributes are found.  
	* Params:
	*	attribute - The attribute type: POSITIONS, COLORS, NORMALS.
	*	offset - The location in the buffer where the data begins.
	*/
	void setVertexAttributePointer(VertexAttribute attribute, unsigned int offset);

	void useShader(ShaderInterface *shader);

	/**
	* Sets whether the rasterizer should scale the point sizes based on their z-distance.
	*/
	void scalePointsByDistance(bool scale);
	
	/*
	* Sets whether the rasterizer will use depth testing on each fragment.
	*/
	void enableDepthTesting(bool test);
	void setDepthFunction(DepthFunction func);
	
	/*
	* Sets whether the rasterizer will cull faces.
	*/
	void enableFaceCulling(bool cull);

	/*
	* Sets which face of a triangle the rasterizer should cull if face culling is enabled.  
	*/
    void cullFace(Face face);

	/*
	* Tells the rasterizer how to determine which side of a triangle is the front face.  This is based upon the ordering of the 
	* vertices (clockwise or counterclockwise).
	*/
    void setFrontFace(WindingOrder order);

	/*
	* Renders an individual pixel to the screen, given its position in window coordinates and final color.
	* Params:
	*	x - The x-axis window coordinate.
	*	y - The y-axis window coordinate.
	*	z - The z window coordinate (fragment depth).
	*	color - The color that the pixel should be set to.
	*/
	void setPixel(int x, int y, float z, int32_t color);

	/*
	* Draws an array of vertices to the screen.
	* Params:
	*	mode - Determines how the vertex data should be interpreted (see RenderMode enum).
	*	first - The starting index.
	*	count - The number of vertices.
	*/
	void drawArrays(RenderMode mode, unsigned int first, unsigned int count);

	/*
	* Params:
	*	mode - Determines how the vertex data should be rendered (see RenderMode enum).
	*	first - The starting index for the element array.
	*	count - The number of indicies.
	*/
	void drawElements(RenderMode mode, unsigned int first, unsigned int count);

private:
	uint32_t* m_Framebuffer;
	float* m_DepthBuffer;

	GraphicsContext m_Context;

	const unsigned int m_WIDTH;
	const unsigned int m_HEIGHT;
	Color m_ClearColor;

	static const unsigned int m_POINT_RADIUS = 3;
	bool m_ScalePoints;

	bool m_DepthTestEnabled;
	DepthFunction m_DepthFunction;

	bool m_FaceCullingEnabled;
	Face m_FaceToCull;
	WindingOrder m_WindingOrder;

	// Clip codes.
	static const int m_INSIDE = 0; // 000000
	static const int m_LEFT = 1;   // 000001
	static const int m_RIGHT = 2;  // 000010
	static const int m_BOTTOM = 4; // 000100
	static const int m_TOP = 8;    // 001000
	static const int m_FRONT = 16; // 010000
    static const int m_BACK = 32;  // 100000

	ClippingPlanes m_ClippingPlanes;

	/*
	* Draws each vertex as a point on the screen.
	* Params:
	*	p0 - The position.
	*	c0 - The color.
	*	n0 - The normal.
	*/
	void drawPoint(glm::vec4 &p0, glm::vec4 &c0, glm::vec4 &n0);

	/*
	* Draws a single line.
	* Algorithm referenced from: http://joshbeam.com/articles/simple_line_drawing
	* Params:
	*	line - The line to draw.
	*/
	void drawLine(Line &line);

	/*
	* Draws a triangle using Barycentric coordinates. 
	* General rasterization algorithm referenced from: http://www.doc.ic.ac.uk/~dfg/graphics/graphics2009/GraphicsLecture06.pdf
	* Barycentric coordinate algorithm referenced from: http://fgiesen.wordpress.com/2013/02/10/optimizing-the-basic-rasterizer/
	* Params:
	*	triangle - The triangle to rasterize.
	*/
	void drawTriangle(const Triangle &triangle);

	/*
	* Computes the signed area of a set of three points.
	* Algorithm referenced from: http://fgiesen.wordpress.com/2013/02/08/triangle-rasterization-in-practice/
	*/
	int computeSignedArea(const glm::vec3 &a, const glm::vec3 &b, const glm::vec3 &c);

	/*
	* Cull a point using a simple bounds test.
	* Params:
	*	p0 - A pointer to the vertex.  If the vertex is culled, the pointer is set to NULL.
	*/
	void clipPoint(glm::vec4 *&p0);

	/*
	* Clips a line segment using the Cohen-Sutherland clipping algorithm.  
	* Algorithm referenced from http://en.wikipedia.org/wiki/Cohen%E2%80%93Sutherland_algorithm
	* Params:
	*	line - A pointer to the line.  If clipping occurs, the vertices are modified to the new values.  If the entire line is culled, set to NULL.
	*/
	void clipLine(Line *&line);

	/*
	* Clips a triangle using the Sutherland-Hodgeman clipping algorithm.
	* Algorithm referenced from http://en.wikipedia.org/wiki/Sutherland%E2%80%93Hodgman_algorithm
	* Params: 
	*	triangle - The triangle to clip.  
	* Returns:
	*	A vector of vertices for clipped triangles.  More than one triangle may be returned, since clipping sometimes produces 
	*	multiple new triangles.  Rendering these new vertices should be done as if it were a triangle strip.
	*/
	std::vector<Triangle> clipTriangle(const Triangle &triangle);

	/*
	* Computes the clip code for a point in 3D space; used for the Cohen-Sutherland clipping algorithm.
	* Params:
	*	x - The point's x clip-coordinates.
	*	y - The point's y clip-coordinates.
	*	z - The point's z clip-coordinates.
	*   w - The extent of clip space (essentially given by the w coordinate on the vectors).
	* Returns:
	*	The clip code associated with the point's location.
	*/
	int calculateClipCode(float x, float y, float z, float w);

	/*
	* Clips a line segment against a given clip plane.
	* Params:
	*	p0 - The first point of the line segment.
	*	p1 - The point that lies outside the clipping bounds and needs to be clipped; the second point of the line segment.
	*	plane - The plane to clip the point against.
	*	a - The interpolant. Modified as a side effect.  This value can be used to average colors.
	* Returns:
	*	The clipped point.
	*/
	glm::vec4 clipLineAgainstPlane(const glm::vec4 &p0, const glm::vec4 &p1, const Plane &plane, float &a);

	/*
	* Checks if a point should be clipped against the near plane.
	* Returns:
	*	true if the point is inside the clipping region (do not clip).
	*/
	bool insideClippingRegion(const glm::vec4 &p);

	/*
	* Converts the given positions from clip space coordinates to window coordinates.
	* Params:
	*	p0 - The point to convert.
	* Returns:
	*	The point in window coordinates.
	*/
	glm::vec3 clipToWindowCoords(const glm::vec4 &p0);

	/*
	* Determines if a fragment should be drawn to the screen by using the set depth test.
	* Params:
	*	inFragDepth - The incoming fragment depth.
	*	bufferDepth - The last depth value stored in the depth buffer.
	* Returns:
	*	true if the fragment passed the depth test.
	*/
	bool testFragmentDepth(float inFragDepth, float bufferDepth);
};

