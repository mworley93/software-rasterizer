#pragma once
#include "ShaderInterface.h"
#include <glm\glm.hpp>

/*
* This class is a basic shader that is based on the Phong reflection model (ambient, diffuse, and specular lighting).  As per most simple 3D shaders, it 
* also performs the model-view-projection transformation on the vertices that is required for perspective rendering.
*/
class PhongShader
	: public ShaderInterface
{
public:
	PhongShader(void);

	~PhongShader(void);

	void vertexShader(glm::vec4 &position, glm::vec4 &color, glm::vec4 &normal);

	void fragmentShader(glm::vec3 &position, glm::vec4 &color, glm::vec4 &normal);

	void setModelViewMatrix(const glm::mat4 &mvp);

	void setViewToClipMatrix(const glm::mat4 &clipMat);

	void setLightPosition(const glm::vec3 &light);

	void setNormalMatrix(const glm::mat4 &normMat);

private:
	glm::mat4 mvMatrix;
	glm::mat4 clipMatrix;
	glm::mat4 normalMatrix;
	glm::vec3 lightPos;
};