#include "Rasterizer.h"

Rasterizer::Rasterizer(const unsigned int width, const unsigned int height) 
	: m_WIDTH(width)
	, m_HEIGHT(height) 
	, m_ClearColor(0.0f, 0.0f, 0.0f, 1.0f)
	, m_ScalePoints(false)
	, m_DepthTestEnabled(false)
	, m_DepthFunction(LESS)
	, m_FaceCullingEnabled(false)
	, m_FaceToCull(BACK)
	, m_WindingOrder(COUNTER_CLOCKWISE)
{	
	m_Framebuffer = new uint32_t[width*height];
	m_DepthBuffer = new float[width*height];
}


Rasterizer::~Rasterizer(void) {
	if (m_Framebuffer) {
		delete [] m_Framebuffer;
	}
	if (m_DepthBuffer) {
		delete [] m_DepthBuffer;
	}
}

uint32_t* Rasterizer::getFramebuffer() {
	return m_Framebuffer;
}

void Rasterizer::setClearColor(Color color) {
	m_ClearColor = color;
}

void Rasterizer::clear() {
	for (unsigned int i = 0; i < (m_WIDTH * m_HEIGHT); ++i) {
		m_Framebuffer[i] = m_ClearColor.toInt32();
		m_DepthBuffer[i] = 1.0f;
	}
}

void Rasterizer::generateBuffer(unsigned int &handle) {
	bool validHandle = false;
	// Generate a new random handle until a valid one is found.
	while (!validHandle) {
		srand (time(NULL));
		handle = rand() + 1;
		if (m_Context.buffers.find(handle) == m_Context.buffers.end()) {
			validHandle = true;
		}
	}
	Buffer *buffer = new Buffer();
	m_Context.buffers[handle] = (buffer);
}

void Rasterizer::bindBuffer(BufferType target, unsigned int handle) {
	if (target == ARRAY_BUFFER) {
		if (handle != 0) {
			std::unordered_map<unsigned int, Buffer*>::iterator iter = m_Context.buffers.find(handle);
			if (iter != m_Context.buffers.end()) {
				m_Context.arrayBuffer = iter->second;
			}
		}
		else {
			m_Context.arrayBuffer == NULL;
		}
	}
	else if (target == ELEMENT_BUFFER) {
		if (handle != 0) {
			std::unordered_map<unsigned int, Buffer*>::iterator iter = m_Context.buffers.find(handle);
			if (iter != m_Context.buffers.end()) {
				m_Context.elementBuffer = iter->second;
			}
		}
		else {
			m_Context.elementBuffer == NULL;
		}
	}
}

void Rasterizer::bufferData(BufferType target, unsigned int count, const void *data) {
	if (target == ARRAY_BUFFER) {
		glm::vec4 *dataCast = (glm::vec4*)data;
		glm::vec4 *pData = new glm::vec4[count];
		std::copy(dataCast, dataCast + count, pData);
		m_Context.arrayBuffer->pData = pData;
	}
	else if (target == ELEMENT_BUFFER) {
		unsigned short *dataCast = (unsigned short*)data;
		unsigned short *pData = new unsigned short[count];
		std::copy(dataCast, dataCast + count, pData);
		m_Context.elementBuffer->pData = pData;
	}
}

void Rasterizer::deleteBuffer(unsigned int handle) {
	std::unordered_map<unsigned int, Buffer*>::iterator iter = m_Context.buffers.find(handle);
	if (handle != 0 && iter != m_Context.buffers.end()) {
		delete iter->second;
		m_Context.buffers.erase(handle);
	}
}

void Rasterizer::setVertexAttributePointer(VertexAttribute attribute, unsigned int offset) {
	if (m_Context.arrayBuffer) {
		if (attribute == POSITIONS) {
			m_Context.positionAttribute = &((glm::vec4*)m_Context.arrayBuffer->pData)[offset];
		}
		else if (attribute == COLORS) {
			m_Context.colorAttribute = &((glm::vec4*)m_Context.arrayBuffer->pData)[offset];
		}
		else if (attribute == NORMALS) {
			m_Context.normalAttribute = &((glm::vec4*)m_Context.arrayBuffer->pData)[offset];
		}
	}
}

void Rasterizer::useShader(ShaderInterface* shader) {
	m_Context.currentShader = shader;
}

void Rasterizer::scalePointsByDistance(bool scale) {
	m_ScalePoints = scale;
}

void Rasterizer::enableDepthTesting(bool test) {
	m_DepthTestEnabled = test;
}

void Rasterizer::setDepthFunction(DepthFunction func) {
	m_DepthFunction = func;
}

void Rasterizer::enableFaceCulling(bool cull) {
	m_FaceCullingEnabled = cull;
}

void Rasterizer::cullFace(Face face) {
	m_FaceToCull = face;
}

void Rasterizer::setFrontFace(WindingOrder order) {
	m_WindingOrder = order;
}

void Rasterizer::setPixel(int x, int y, float z, int32_t color) {
	if (x >= 0 && x < m_WIDTH && y >= 0 && y < m_HEIGHT) {
        int index = x + y * m_WIDTH;
		if (m_DepthTestEnabled) {
			if (testFragmentDepth(z, m_DepthBuffer[index])) {
				m_Framebuffer[index] = color;
				m_DepthBuffer[index] = z;
			}
		}
		else {
			m_Framebuffer[index] = color;
		}
	}
}


bool Rasterizer::testFragmentDepth(float inFragDepth, float bufferDepth) {
	switch (m_DepthFunction) {
	case ALWAYS:
		return true;
	case NEVER:
		return false;
	case LESS:
		return inFragDepth < bufferDepth;
	case GREATER:
		return inFragDepth > bufferDepth;
	case LEQUAL:
		return inFragDepth <= bufferDepth;
	case GEQUAL:
		return inFragDepth >= bufferDepth;
	case EQUAL:
		return inFragDepth == bufferDepth;
	case NOT_EQUAL:
		return inFragDepth != bufferDepth;
	default:
		return true;
	}
}

void Rasterizer::drawArrays(RenderMode mode, unsigned int first, unsigned int count) {
	ShaderInterface* shader = m_Context.currentShader;
	glm::vec4* vertices = m_Context.positionAttribute;
	glm::vec4* colors = m_Context.colorAttribute;
	glm::vec4* normals = m_Context.normalAttribute;
	if (!vertices || !shader || !normals) {
		return;
	}
	for (unsigned int i = first; i < count; ++i) {
		if (mode == POINTS) {
			glm::vec4 v0 = vertices[i];
			glm::vec4 c0 = colors[i];
			glm::vec4 n0 = normals[i];
			shader->vertexShader(v0, c0, n0);
			drawPoint(v0, c0, n0);
		}
		else if (mode == LINES) {
			if ((first + i) % 2 == 0 && i+1 != count) {
				Line line;
				line.setPositions(vertices[i], vertices[i+1]);
				line.setColors(colors[i], colors[i+1]);
				line.setNormals(normals[i], normals[i+1]);
				shader->vertexShader(line.p0, line.c0, line.n0);
				shader->vertexShader(line.p1, line.c1, line.n1);
				drawLine(line);
			}
		}
		else if (mode == LINE_STRIP) {
			if (i != (count - 1)) {
				Line line;
				line.setPositions(vertices[i], vertices[i+1]);
				line.setColors(colors[i], colors[i+1]);
				line.setNormals(normals[i], normals[i+1]);
				shader->vertexShader(line.p0, line.c0, line.n0);
				shader->vertexShader(line.p1, line.c1, line.n1);
				drawLine(line);
			}
		}
		else if (mode == TRIANGLES) {
			if ((first + i) % 3 == 0) {
				Triangle triangle;
				glm::vec4 v0 = vertices[i]; glm::vec4 v1 = vertices[i+1]; glm::vec4 v2 = vertices[i+2];
				glm::vec4 c0 = colors[i]; glm::vec4 c1 = colors[i+1]; glm::vec4 c2 = colors[i+2];
				glm::vec4 n0 = normals[i]; glm::vec4 n1 = normals[i+1]; glm::vec4 n2 = normals[i+2];
				shader->vertexShader(v0, c0, n0);
				shader->vertexShader(v1, c1, n1);
				shader->vertexShader(v2, c2, n2);
			
				if (m_FaceCullingEnabled) {
					if (m_FaceToCull == FRONT_AND_BACK) {
						return;
					}
					else if (m_FaceToCull == BACK) {
						if (m_WindingOrder == COUNTER_CLOCKWISE) {
							// Ordering seems backwards here...this is because after the transformation from NDC to window coordinates, 
							// the y-direction is flipped.
							triangle.setPositions(v0, v2, v1);
							triangle.setColors(c0, c2, c1);
							drawTriangle(triangle);
						}
						else {
							triangle.setPositions(v0, v1, v2);
							triangle.setColors(c0, c1, c2);
							drawTriangle(triangle);
						}
					}
					else {
						if (m_WindingOrder == COUNTER_CLOCKWISE) { 
							triangle.setPositions(v0, v1, v2);
							triangle.setColors(c0, c1, c2);
							drawTriangle(triangle);
						}
						else {
							triangle.setPositions(v0, v2, v1);
							triangle.setColors(c0, c2, c1);
							drawTriangle(triangle);
						}
					}
				}
				else {
					triangle.setPositions(v0, v1, v2);
					triangle.setColors(c0, c1, c2);
					drawTriangle(triangle);
					triangle.setPositions(v0, v2, v1);
					triangle.setColors(c0, c2, c1);
					drawTriangle(triangle);
				}
			}
		}
	}
}

void Rasterizer::drawElements(RenderMode mode, unsigned int first, unsigned int count) {
	unsigned short* indices = (unsigned short*)m_Context.elementBuffer->pData;
	glm::vec4* vertices = m_Context.positionAttribute;
	ShaderInterface* shader = m_Context.currentShader;
	glm::vec4* colors = m_Context.colorAttribute;
	glm::vec4* normals = m_Context.normalAttribute;
	if (!vertices || !shader || !normals || !indices) {
		return;
	}
	for (unsigned int i = first; i < count; ++i) {
		if (mode == POINTS) {
			glm::vec4 v0 = vertices[indices[i]];
			glm::vec4 c0 = colors[indices[i]];
			glm::vec4 n0 = normals[indices[i]];
			shader->vertexShader(v0, c0, n0);
			drawPoint(v0, c0, n0);
		}
		else if (mode == LINES) {
			if ((first + i) % 2 == 0 && i+1 != count) {
				Line line;
				line.setPositions(vertices[indices[i]], vertices[i+1]);
				line.setColors(colors[indices[i]], colors[indices[i+1]]);
				line.setNormals(normals[indices[i]], normals[indices[i+1]]);
				shader->vertexShader(line.p0, line.c0, line.n0);
				shader->vertexShader(line.p1, line.c1, line.n1);
				drawLine(line);
			}
		}
		else if (mode == LINE_STRIP) {
			if (i != (count - 1)) {
				Line line;
				line.setPositions(vertices[indices[i]], vertices[indices[i+1]]);
				line.setColors(colors[indices[i]], colors[indices[i+1]]);
				line.setNormals(normals[indices[i]], normals[indices[i+1]]);
				shader->vertexShader(line.p0, line.c0, line.n0);
				shader->vertexShader(line.p1, line.c1, line.n1);
				drawLine(line);
			}
		}
		else if (mode == TRIANGLES) {
			if ((first + i) % 3 == 0) {
				Triangle triangle;
				glm::vec4 v0 = vertices[indices[i]]; 
				glm::vec4 v1 = vertices[indices[i+1]]; 
				glm::vec4 v2 = vertices[indices[i+2]];
				glm::vec4 c0 = colors[indices[i]]; glm::vec4 c1 = colors[indices[i+1]]; glm::vec4 c2 = colors[indices[i+2]];
				glm::vec4 n0 = normals[indices[i]]; glm::vec4 n1 = normals[indices[i+1]]; glm::vec4 n2 = normals[indices[i+2]];
				shader->vertexShader(v0, c0, n0);
				shader->vertexShader(v1, c1, n1);
				shader->vertexShader(v2, c2, n2);
			
				if (m_FaceCullingEnabled) {
					if (m_FaceToCull == FRONT_AND_BACK) {
						return;
					}
					else if (m_FaceToCull == BACK) {
						if (m_WindingOrder == COUNTER_CLOCKWISE) {
							// Ordering seems backwards here...this is because after the transformation from NDC to window coordinates, 
							// the y-direction is flipped.
							triangle.setPositions(v0, v2, v1);
							triangle.setColors(c0, c2, c1);
							drawTriangle(triangle);
						}
						else {
							triangle.setPositions(v0, v1, v2);
							triangle.setColors(c0, c1, c2);
							drawTriangle(triangle);
						}
					}
					else {
						if (m_WindingOrder == COUNTER_CLOCKWISE) { 
							triangle.setPositions(v0, v1, v2);
							triangle.setColors(c0, c1, c2);
							drawTriangle(triangle);
						}
						else {
							triangle.setPositions(v0, v2, v1);
							triangle.setColors(c0, c2, c1);
							drawTriangle(triangle);
						}
					}
				}
				else {
					triangle.setPositions(v0, v1, v2);
					triangle.setColors(c0, c1, c2);
					drawTriangle(triangle);
					triangle.setPositions(v0, v2, v1);
					triangle.setColors(c0, c2, c1);
					drawTriangle(triangle);
				}
			}
		}
	}
}

void Rasterizer::drawPoint(glm::vec4 &p0, glm::vec4 &c0, glm::vec4 &n0) {
	glm::vec4 *clippedVertex = &p0;
	clipPoint(clippedVertex); 
	if (!clippedVertex) {
		return;
	}
	glm::vec3 vertex = clipToWindowCoords(*clippedVertex);
	unsigned int radius = m_POINT_RADIUS;
	if (m_ScalePoints) {
		radius = glm::ceil((1-vertex.z)*3);
	}
	for (unsigned int j = 0; j < (2 * radius + 1); ++j) {
		for (unsigned int k = 0; k < (2 * radius + 1); ++k) {
			int pixelX = (vertex.x - radius) + k;
			int pixelY = (vertex.y - radius) + j;

			// Fragment shading.
			glm::vec3 position(pixelX, pixelY, vertex.z);
			glm::vec4 color(c0.x, c0.y, c0.z, c0.w);
			m_Context.currentShader->fragmentShader(position, color, n0);
			
			setPixel(position.x, position.y, position.z, Color(color.x, color.y, color.z, color.w).toInt32());
		}
	}
}

void Rasterizer::clipPoint(glm::vec4 *&vertex) {
	if (vertex->w < 0) { vertex = NULL; return;}
	if (vertex->x < -vertex->w || vertex->x > vertex->w) { vertex = NULL; return; }
	if (vertex->y < -vertex->w || vertex->y > vertex->w) { vertex = NULL; return; }
	if (vertex->z < -vertex->w || vertex->z > vertex->w) { vertex = NULL; return; }
}

void Rasterizer::drawLine(Line &line) {
	Line *linePtr = &line;
	clipLine(linePtr); 
	if (!linePtr) {
		return;
	}

	glm::vec3 v1 = clipToWindowCoords(linePtr->p0);
	glm::vec3 v2 = clipToWindowCoords(linePtr->p1);
	glm::vec4 c0 = linePtr->c0;
	glm::vec4 c1 = linePtr->c1;
	glm::vec4 n0 = linePtr->n0;
	glm::vec4 n1 = linePtr->n1;

	float dx = v2.x - v1.x;
	float dy = v2.y - v1.y;

	if (dx == 0.0f && dy == 0.0f) {	
		m_Context.currentShader->fragmentShader(v1, c1, n1);
		setPixel(v1.x, v1.y, v1.z, Color(c1.x, c1.y, c1.z, c1.w).toInt32());
	}

	else if (fabs(dx) > fabs(dy)) {
		float xmin, xmax;
		if(v1.x < v2.x) {
            xmin = v1.x;
            xmax = v2.x;
        } 
		else {
            xmin = v2.x;
            xmax = v1.x;
        }
		float slope = dy / dx;
		float a = 0;
		float interp = 0;
        for(float x = xmin; x <= xmax; x += 1.0f) {
            float y = (x - v1.x) * slope + v1.y;
			// Fragment shading.
			glm::vec4 interpolatedColor = glm::mix(c0, c1, a);
			glm::vec3 position(x, y, v1.z);
			glm::vec4 interpolatedNormal = glm::mix(n0, n1, a);
			m_Context.currentShader->fragmentShader(position, interpolatedColor, interpolatedNormal);
			
			setPixel(position.x, position.y, position.z, Color(interpolatedColor.x, interpolatedColor.y, interpolatedColor.z, interpolatedColor.w).toInt32());
			interp += 1.0f;
			a = interp / (xmax-xmin);
        }
	}
	else {
        float ymin, ymax;
        if(v1.y < v2.y) {
            ymin = v1.y;
            ymax = v2.y;
        } else {
            ymin = v2.y;
            ymax = v1.y;
        }

        // draw line in terms of x slope
        float slope = dx / dy;
		float a = 0;
		float interp = 0;
        for(float y = ymin; y <= ymax; y += 1.0f) {
            float x = v1.x + ((y - v1.y) * slope);
			// Fragment shading.
			glm::vec4 interpolatedColor = glm::mix(c0, c1, a);
			glm::vec3 position(x, y, v1.z);
			glm::vec4 interpolatedNormal = glm::mix(n0, n1, a);
			m_Context.currentShader->fragmentShader(position, interpolatedColor, interpolatedNormal);
			
			setPixel(position.x, position.y, position.z, Color(interpolatedColor.x, interpolatedColor.y, interpolatedColor.z, interpolatedColor.w).toInt32());
			interp += 1.0f;
			a = interp / (ymax-ymin);
        }
    }
}

void Rasterizer::drawTriangle(const Triangle &triangle) {
	std::vector<Triangle> clippedTriangles = clipTriangle(triangle);
	if (clippedTriangles.size() < 1) {
		return;
	}

	// Rasterize
	for (int i = 0; i < clippedTriangles.size(); i++) {
		glm::vec3 a = clipToWindowCoords(clippedTriangles[i].positions[0]);
		glm::vec3 b = clipToWindowCoords(clippedTriangles[i].positions[1]);
		glm::vec3 c = clipToWindowCoords(clippedTriangles[i].positions[2]);
		glm::vec4 c0 = clippedTriangles[i].colors[0];
		glm::vec4 c1 = clippedTriangles[i].colors[1];
		glm::vec4 c2 = clippedTriangles[i].colors[2];
		glm::vec4 n0 = clippedTriangles[i].normals[0];
		glm::vec4 n1 = clippedTriangles[i].normals[1];
		glm::vec4 n2 = clippedTriangles[i].normals[2];

		// Set up triangle bounding box.
		int minX = glm::min(a.x, glm::min(b.x, c.x));
		int minY = glm::min(a.y, glm::min(b.y, c.y));
		int maxX = glm::max(a.x, glm::max(b.x, c.x));
		int maxY = glm::max(a.y, glm::max(b.y, c.y));

		// Clip against screen bounds
		minX = glm::max(minX, 0);
		minY = glm::max(minY, 0);
		maxX = glm::min(maxX, (int)m_WIDTH - 1);
		maxY = glm::min(maxY, (int)m_HEIGHT - 1);

		// Set up the triangle vectors.
		int A01 = a.y - b.y; 
		int B01 = b.x - a.x;
		int A12 = b.y - c.y; 
		int B12 = c.x - b.x;
		int A20 = c.y - a.y; 
		int B20 = a.x - c.x;

		// Compute barycentric coordinates at the intial point.
		glm::vec3 p(minX, minY, 0.0f);
		int w0_row = computeSignedArea(b, c, p);
		int w1_row = computeSignedArea(c, a, p);
		int w2_row = computeSignedArea(a, b, p);
		float area = computeSignedArea(a, b, c);


		// Rasterize
		for (p.y = minY; p.y <= maxY; p.y++) {
			int w0 = w0_row;
			int w1 = w1_row;
			int w2 = w2_row;

			for (p.x = minX; p.x <= maxX; p.x++) {  
				// If p is on or inside all edges, render pixel.
				if (w0 >= 0 && w1 >= 0 && w2 >= 0) {
					// Interpolate depth, color, etc.
					float zvalue = (a.z*w0 + b.z*w1 + c.z*w2)/area;
					float r = (c0.x* w0 + c1.x * w1 + c2.x * w2) / area;
					float g = (c0.y * w0 + c1.y * w1 + c2.y * w2) / area;
					float b = (c0.z * w0 + c1.z * w1 + c2.z * w2) / area;
					float a = (c0.w * w0 + c1.w * w1 + c2.w * w2) / area;
					float nX = (n0.x* w0 + n1.x * w1 + n2.x * w2) / area;
					float nY = (n0.y* w0 + n1.y * w1 + n2.y * w2) / area;
					float nZ = (n0.z* w0 + n1.z * w1 + n2.z * w2) / area;

					// Fragment shading.
					glm::vec3 position(p.x, p.y, zvalue);
					glm::vec4 color(r, g, b, a);
					glm::vec4 normal(nX, nY, nZ, 1.0f);

					m_Context.currentShader->fragmentShader(position, color, normal);
					setPixel(position.x, position.y, position.z, Color(color.x, color.y, color.z, color.w).toInt32());
				}

				// Step to the right.
				w0 += A12;
				w1 += A20;
				w2 += A01;
			}

			// Step in the y direction.
			w0_row += B12;
			w1_row += B20;
			w2_row += B01;
		}
	}
}

int Rasterizer::computeSignedArea(const glm::vec3 &a, const glm::vec3 &b, const glm::vec3 &p) {
    return (b.x-a.x)*(p.y-a.y) - (b.y-a.y)*(p.x-a.x);
}

void Rasterizer::clipLine(Line *&line) {
	std::vector<glm::vec4> acceptedSegments;
	int firstCode, secondCode;
	firstCode = calculateClipCode(line->p0.x, line->p0.y, line->p0.z, line->p0.w);
	secondCode = calculateClipCode(line->p1.x, line->p1.y, line->p1.z, line->p1.w);

	while(true) {
		// Trivially accept line segment.  Bitwise OR returns 0 if both codes are 000000.
		if (!(firstCode | secondCode)) {
			break;
		}
		// Trivially discard line segment.  Bitwise AND evaluating to 1 means that the two points shared a region, so
		// their joining line could not pass through the accepting region.
		else if (firstCode & secondCode) {
			line = NULL;
			break;
		}
		// A line segment needs to be clipped.
		else {
			// At least one endpoint is outside the clip rectangle; pick it.
			int outCode = firstCode ? firstCode : secondCode;
			glm::vec4 clippedPoint;
			glm::vec4 interpolatedColor;
			glm::vec4 interpolatedNormal;
			glm::vec4 firstVertex = line->p0;
			glm::vec4 secondVertex = line->p1;
			glm::vec4 firstColor = line->c0;
			glm::vec4 secondColor = line->c1;
			glm::vec4 firstNormal = line->n0;
			glm::vec4 secondNormal = line->n1;
			float a;
			if (outCode == firstCode) {
				secondVertex = line->p0;
				firstVertex = line->p1;
				secondColor = line->c0;
				firstColor = line->c1;
				firstNormal = line->n0;
				secondNormal = line->n1;
			}
 
			// Now find the intersection point by clipping the line segment against the clipping plane.
			if (outCode & m_BACK) {   
				clippedPoint = clipLineAgainstPlane(firstVertex, secondVertex, m_ClippingPlanes.far, a);
			} 
			else if (outCode & m_FRONT) { 
				clippedPoint = clipLineAgainstPlane(firstVertex, secondVertex, m_ClippingPlanes.near, a);
			} 
			else if (outCode & m_TOP) { 
				clippedPoint = clipLineAgainstPlane(firstVertex, secondVertex, m_ClippingPlanes.top, a);
			} 
			else if (outCode & m_BOTTOM) { 
				clippedPoint = clipLineAgainstPlane(firstVertex, secondVertex, m_ClippingPlanes.bottom, a);
			} 
			else if (outCode & m_RIGHT) { 
				clippedPoint = clipLineAgainstPlane(firstVertex, secondVertex, m_ClippingPlanes.right, a);
			} 
			else if (outCode & m_LEFT) {  
				clippedPoint = clipLineAgainstPlane(firstVertex, secondVertex, m_ClippingPlanes.left, a);
			}

			interpolatedColor = glm::mix(firstColor, secondColor, a);
			interpolatedNormal = glm::mix(firstNormal, secondNormal, a);
 
			// Set the coordinates of the newly calculated vertex.
			if (outCode == firstCode) {
				line->p0 = clippedPoint;
				line->c0 = interpolatedColor;
				line->n0 = interpolatedNormal;
				firstCode = calculateClipCode(line->p0.x, line->p0.y, line->p0.z, line->p0.w);
			} 
			else {
				line->p1 = clippedPoint;
				line->c1 = interpolatedColor;
				line->n1 = interpolatedNormal;
				secondCode = calculateClipCode(line->p1.x, line->p1.y, line->p1.z, line->p1.w);
			}
		}
	}
}

int Rasterizer::calculateClipCode(float x, float y, float z, float w) {
	int code = 0;

	if (w > 0) {
		code += (z > w ? m_BACK: 0);
		code += (z < -w ? m_FRONT: 0);
		code += (y > w ? m_TOP: 0);
		code += (y < -w ? m_BOTTOM: 0);
		code += (x > w ? m_RIGHT: 0);
		code += (x < -w ? m_LEFT: 0);
	}
	else {
		code += m_FRONT;
		code += (y > -w ? m_TOP: 0);
		code += (y < w ? m_BOTTOM: 0);
		code += (x > -w ? m_RIGHT: 0);
		code += (x < w ? m_LEFT: 0);
	}
	return code;
}

std::vector<Triangle> Rasterizer::clipTriangle(const Triangle &triangle) {
	std::vector<glm::vec4> clipped;
	std::vector<glm::vec4> interpolatedColors;
	std::vector<glm::vec4> interpolatedNormals;
	float a;

	// A triangle has 3 edges, so traverse all three...
	for (int i = 0; i < 3; i++) {
		// Grab an edge.
		glm::vec4 v0 = triangle.positions[i%3];
		glm::vec4 v1 = triangle.positions[(i+1)%3];
		glm::vec4 c0 = triangle.colors[i%3];
		glm::vec4 c1 = triangle.colors[(i+1)%3];
		glm::vec4 n0 = triangle.normals[i%3];
		glm::vec4 n1 = triangle.normals[(i+1)%3];
		if (insideClippingRegion(v0)) {
			// If both points of the edge are inside, add the second point.
			if (insideClippingRegion(v1)) {
				clipped.push_back(v1);
				interpolatedColors.push_back(c1);
				interpolatedNormals.push_back(n1);
			}
			// If the first point of the edge is inside and the second outside, add the intersection.
			else {
				clipped.push_back(clipLineAgainstPlane(v0, v1, m_ClippingPlanes.near, a));
				interpolatedColors.push_back(glm::mix(c0, c1, a));
				interpolatedNormals.push_back(glm::mix(n0, n1, a));
			}
		}
		else {
			// If the first point is outside and the second inside, add both the intersection and the second point.
			if (insideClippingRegion(v1)) {
				clipped.push_back(clipLineAgainstPlane(v1, v0, m_ClippingPlanes.near, a));
				interpolatedColors.push_back(glm::mix(c1, c0, a));
				interpolatedNormals.push_back(glm::mix(n1, n0, a));
				clipped.push_back(v1);
				interpolatedColors.push_back(c1);
				interpolatedNormals.push_back(n1);
			}
		}
	}

	// If multiple triangles were produced by the clipping stage, then this will triangulate the polygon.  It should be convex, 
	// so triangulation is trivial (just draw a diagonal from the starting vertex to all other unconnected vertices).
	std::vector<Triangle> clippedTriangles;
	if (clipped.size() >= 3) {
		for (int i = 1; i < clipped.size()-1; ++i) {
			Triangle subTriangle;
			subTriangle.setPositions(clipped[0], clipped[i], clipped[i+1]);
			subTriangle.setColors(interpolatedColors[0], interpolatedColors[i], interpolatedColors[i+1]);
			subTriangle.setNormals(interpolatedNormals[0], interpolatedNormals[i], interpolatedNormals[i+1]);
			clippedTriangles.push_back(subTriangle);
		}
	}

	return clippedTriangles;
}

bool Rasterizer::insideClippingRegion(const glm::vec4 &p) {
	if (p.w > 0) {
		if (p.z < -p.w) { 
			return false; 
		}
		else {
			return true;
		}
	}
	else {
		return false;
	}
}


glm::vec4 Rasterizer::clipLineAgainstPlane(const glm::vec4 &p0, const glm::vec4 &p1, const Plane &plane, float &a) {
	float d0 = glm::dot(p0, plane.normal);
	float d1 = glm::dot(p1, plane.normal);
	a = d0/(d0-d1);
	glm::vec4 pc = (1-a)*p0 + a*p1;
	return pc;
}

glm::vec3 Rasterizer::clipToWindowCoords(const glm::vec4 &vertex) {
	glm::vec3 windowCoords;
	float ndcX = vertex.x / vertex.w;
	float ndcY = vertex.y / vertex.w;
	float ndcZ = vertex.z / vertex.w;
	windowCoords.x = glm::round(( ndcX + 1 ) * (m_WIDTH-1) * 0.5);
	windowCoords.y = glm::round((m_HEIGHT-1) - (( ndcY + 1 ) * (m_HEIGHT-1)* 0.5));
	windowCoords.z = ( ndcZ + 1) * 0.5;
	return windowCoords; 
}