#include "Camera.h"


Camera::Camera(void)
	: m_Eye((0,0,0))
	, m_Up((0,1,0))
	, m_Center((0,0,-1)) {
}


Camera::~Camera(void) {
}

void Camera::lookAt(const glm::vec3 &eye, const glm::vec3 &center, const glm::vec3 &up) {
	m_Eye = eye;
	m_Center = center;
	m_Up = up;
}

glm::vec3 Camera::getOrthoUpVector() {
	glm::vec3 upDir = glm::normalize(m_Up);
	glm::vec3 lookDir = glm::normalize(m_Center - m_Eye);
	glm::vec3 rightDir = glm::normalize(glm::cross(lookDir, upDir));
	return glm::cross(rightDir, lookDir);
}

glm::vec3 Camera::getSideVector() {
	glm::vec3 upDir = glm::normalize(m_Up);
	glm::vec3 lookDir = glm::normalize(m_Center - m_Eye);
	return glm::normalize(glm::cross(lookDir, upDir));
}

glm::vec3 Camera::getLookVector() {
	return glm::normalize(m_Center - m_Eye);
}

void Camera::move(Movement direction, float speed) {
	glm::vec3 translation;
	if (direction == LEFT) {
		translation = -getSideVector();
	}
	else if (direction == RIGHT) {
		translation = getSideVector();
	}
	else if (direction == UP) {
		translation = getOrthoUpVector();
	}
	else if (direction == DOWN) {
		translation = -getOrthoUpVector();
	}
	else if (direction == BACKWARD) {
		translation = -getLookVector();
	}
	else if (direction == FORWARD) { 
		translation = getLookVector();
	}
	
	translation = translation * m_Orientation * speed;
	m_Eye += translation;
	m_Center += translation;
}

void Camera::rotate(const glm::vec3 &axis, float angle) {
	float radians = glm::radians(angle);
	glm::vec3 nAxis = glm::normalize(axis) * sinf(radians/2.0f);
	float scalar = cosf(radians/2.0f);
	m_Orientation = glm::fquat(scalar, nAxis.x, nAxis.y, nAxis.z) * m_Orientation;
}

glm::mat4 Camera::getViewMatrix() {
	glm::vec3 upDir = glm::normalize(m_Up);
	glm::vec3 lookDir = glm::normalize(m_Center - m_Eye);
	glm::vec3 rightDir = glm::normalize(glm::cross(lookDir, upDir));
	glm::vec3 orthoUpDir = glm::cross(rightDir, lookDir);

	glm::mat4 rotation(1.0f);
	rotation[0] = glm::vec4(rightDir, 0.0f);
	rotation[1] = glm::vec4(orthoUpDir, 0.0f);
	rotation[2] = glm::vec4(-lookDir, 0.0f);

	rotation = glm::transpose(rotation);

	glm::mat4 translation(1.0f);
	translation[3] = glm::vec4(-m_Eye, 1.0f);
	return  rotation * glm::mat4_cast(m_Orientation) * translation;
}

glm::mat4 Camera::getOrthoProjection(const float &left, const float &right, const float &bottom, const float &top, 
	const float &zNear, const float &zFar) {
	glm::mat4 projMat(1.0f);
	projMat[0][0] = 2/(right - left);
	projMat[1][1] = 2/(top - bottom);
	projMat[2][2] = -2/(zFar - zNear);
	projMat[3][0] = -(right + left)/(right - left);
	projMat[3][1] = -(top + bottom)/(top - bottom);
	projMat[3][2] = -(zFar + zNear)/(zFar - zNear);
	return projMat;
}

glm::mat4 Camera::getPerspectiveProjection(const float &fov, const float &aspect, const float &zNear, const float &zFar) {
	glm::mat4 projMat(1.0f);
	float frustumScale =  1 / tan(0.5f * fov);
	projMat[0][0] = frustumScale / aspect;
	projMat[1][1] = frustumScale;
	projMat[2][2] = (zFar + zNear) / (zNear - zFar);
	projMat[3][2] = (2 * zFar * zNear) / (zNear - zFar);
	projMat[2][3] = -1.0f;
	return projMat;
}