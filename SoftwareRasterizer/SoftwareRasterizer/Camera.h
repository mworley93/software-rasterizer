#pragma once

#include <glm\glm.hpp>
#include <glm\gtx\quaternion.hpp>

/*
* This class represents a camera in 3D space.  It defines what the user is "looking at," as well as the view distance and 
* field of view.  
*/
class Camera
{
public:
	enum Movement {
		LEFT, RIGHT, UP, DOWN, FORWARD, BACKWARD,
	};

	/*
	* Default constructor.  Assigns the camera position and orientation to the default values: the camera position (eye) is located 
	* at the origin, the view direction (center) is down the -z axis, and the up vector is along the +y axis.
	*/
	Camera();

	~Camera(void);

	/*
	* Assigns a new position and orientation to the camera.
	* Params:
	*	eye - The camera position.
	*	center - The target position that the camera is looking at.
	*	up - The up orientation (the 'roll' of the camera).
	*/
	void lookAt(const glm::vec3 &eye, const glm::vec3 &center, const glm::vec3 &up);

	void setPosition(const glm::vec3 &eye);
	void setTarget(const glm::vec3 &center);
	void setUpVector(const glm::vec3 &up);

	glm::vec3 getOrthoUpVector();
	glm::vec3 getSideVector();
	glm::vec3 getLookVector();

	/*
	* Translates the camera the number of units given in each direction.
	*/
	void move(Movement direction, float speed);

	/*
	* Rotates the camera about an arbitrary axis using quaternions.
	* Params:
	*	axis - The axis to rotate about.
	*	angle - The angle of rotation.
	*/
	void rotate(const glm::vec3 &axis, float angle);

	/*
	* Returns the view transform, calculated from the position and orientation of the camera.  Use this to transform 
	* vertices from world to camera space.
	*/
	glm::mat4 getViewMatrix();

	/*
	* Returns an orthographic projection matrix.
	* Params:
	*	left - The left viewing boundary.
	*	right - The right viewing boundary.
	*	bottom - The bottom viewing boundary.
	*	top - The top viewing boundary.
	*	zNear - The near plane.
	*	zFar - The far plane.
	* Returns:
	*	A 4x4 orthographic projection matrix.
	*/
	glm::mat4 getOrthoProjection(const float &left, const float &right, const float &bottom, const float &top, 
		const float &zNear, const float &zFar);

	/*
	* Returns a perspective projection matrix.
	* Params:
	*	fov - The field of view angle.
	*	aspect - The aspect ratio of the window.
	*	zNear - The near plane.
	*	zFar - The far plane.
	* Returns:
	*	A 4x4 perspective projection matrix.
	*/
	glm::mat4 getPerspectiveProjection(const float &fov, const float &aspect, const float &zNear, const float &zFar);

private:
	glm::vec3 m_Eye;
	glm::vec3 m_Center;
	glm::vec3 m_Up;
	glm::fquat m_Orientation;
};

