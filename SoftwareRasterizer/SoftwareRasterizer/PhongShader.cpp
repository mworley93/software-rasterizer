#include "PhongShader.h"


PhongShader::PhongShader(void)
{
}


PhongShader::~PhongShader(void)
{
}

void PhongShader::vertexShader(glm::vec4 &position, glm::vec4 &color, glm::vec4 &normal) {
	glm::vec3 lightDir = glm::normalize(glm::vec3(lightPos.x, lightPos.y, lightPos.z) - glm::vec3(position.x, position.y, position.z));
	glm::vec3 normCamSpace = glm::normalize(glm::vec3(normal.x, normal.y, normal.z));
	float lambertFactor = glm::dot(normCamSpace, lightDir);
	lambertFactor = glm::clamp(lambertFactor, 0.0f, 1.0f);

	position = clipMatrix * mvMatrix * position;
	glm::vec4 ambient = glm::vec4(0.1, 0.1, 0.1, 1.0f);
	color = (color * lambertFactor) + color * ambient;
}

void PhongShader::fragmentShader(glm::vec3 &position, glm::vec4 &color, glm::vec4 &normal) {
}

void PhongShader::setModelViewMatrix(const glm::mat4 &mv) {
	mvMatrix = mv;
}

void PhongShader::setViewToClipMatrix(const glm::mat4 &clipMat) {
	clipMatrix = clipMat;
}

void PhongShader::setNormalMatrix(const glm::mat4 &normMat) {
	normalMatrix = normMat;
}

void PhongShader::setLightPosition(const glm::vec3 &light) {
	lightPos = light;
}