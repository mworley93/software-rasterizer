#pragma once
#include <glm\glm.hpp>

/*
* This interface is used to specify behavior of a simple programmable shader.  Two functions are required to be implemented, and 
* these represent vertex shaders and fragment shaders that are called at key points during rendering.
*/
class ShaderInterface
{
public:
	ShaderInterface(void) {};

	// Default destructor.
	virtual ~ShaderInterface(void) {};

	/*
	* This method acts as a vertex shader.  It should be called on each vertex before clipping.  
	* Params:
	*	position - The vertex position (x, y, z, w).
	*	color - The vertex color (a, r, g, b);
	*	normal - The normal vector (x, y, z, w)
	*/
	virtual void vertexShader(glm::vec4 &position, glm::vec4 &color, glm::vec4 &normal) = 0;

	/*
	* This method acts as a fragment shader.  It should be called on each fragment after the translated to window coordinates.
	* Params:
	*	position - The vertex position (x, y, z) in window coordinates.
	*	color - The vertex color (a, r, g, b);
	*	normal - The normal vector
	*/
	virtual void fragmentShader(glm::vec3 &position, glm::vec4 &color, glm::vec4 &normal) = 0;
};