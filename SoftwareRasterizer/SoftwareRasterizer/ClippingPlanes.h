#pragma once 

#include "Plane.h"

struct ClippingPlanes {
	Plane left, right, top, bottom, near, far;

	ClippingPlanes()
		: right(Plane(glm::vec4(-1, 0, 0, 1)))
		, left(Plane(glm::vec4(1, 0, 0, 1)))
		, top(Plane(glm::vec4(0, -1, 0, 1)))
		, bottom(Plane(glm::vec4(0, 1, 0, 1)))
		, far(Plane(glm::vec4(0, 0, -1, 1)))
		, near(Plane(glm::vec4(0, 0, 1, 1)))
	{}
};