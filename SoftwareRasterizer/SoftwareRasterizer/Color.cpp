#include "Color.h"

Color::Color(void) {
	m_R = 1.0f;
	m_G = 1.0f;
	m_B = 1.0f;
	m_A = 1.0f;
}

Color::Color(float r, float g, float b, float a) {
	if (r > 1.0f) { r = 1.0f; }
	if (g > 1.0f) { g = 1.0f; }
	if (b > 1.0f) { b = 1.0f; }
	if (a > 1.0f) { a = 1.0f; }
	if (r < 0.0f) { r = 0.0f; }
	if (g < 0.0f) { g = 0.0f; }
	if (b < 0.0f) { b = 0.0f; }
	if (a < 0.0f) { a = 0.0f; }
	m_R = floor(r == 1.0 ? 255 : r * 256.0);
	m_G = floor(g == 1.0 ? 255 : g * 256.0);
	m_B = floor(b == 1.0 ? 255 : b * 256.0);
	m_A = floor(a == 1.0 ? 255 : a * 256.0);
}

/*
* Initializes the individual colors with the given integers in the interval [0,255].
*/
Color::Color(uint8_t r, uint8_t g, uint8_t b, uint8_t a)
	: m_R(r)
	, m_G(g)
	, m_B(b)
	, m_A(a)
{}

uint32_t Color::toInt32() {
	uint32_t argb = m_A;
	argb = (argb << 8) + m_R;
	argb = (argb << 8) + m_G;
	argb = (argb << 8) + m_B;
	return argb;
}