#pragma once
#include <glm\glm.hpp>

/*
* This simple data structure represents a plane in homogenous coordinates.  
*/
struct Plane {
	Plane(glm::vec4 n)
		: normal(n)
	{}
	glm::vec4 normal;
};

