#include <glm\glm.hpp>

/*
* This structure represents a simple triangle.  It contains an array of 3 vertices, normals, and colors.
*/
struct Triangle {

	void setPositions(glm::vec4 a, glm::vec4 b, glm::vec4 c) {
		positions[0] = a;
		positions[1] = b;
		positions[2] = c;
	}

	void setColors(glm::vec4 a, glm::vec4 b, glm::vec4 c) {
		colors[0] = a;
		colors[1] = b;
		colors[2] = c;
	}

	void setNormals(glm::vec4 a, glm::vec4 b, glm::vec4 c) {
		normals[0] = a;
		normals[1] = b;
		normals[2] = c;
	}

	// Positions
	glm::vec4 positions[3];

	// Colors
	glm::vec4 colors[3];

	// Normals
	glm::vec4 normals[3];
};