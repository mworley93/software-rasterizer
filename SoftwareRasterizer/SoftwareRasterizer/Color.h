#ifndef Color_H
#define Color_H

#include <stdint.h>
#include <cmath>

/*
* This class represents a combined color value that can be written to the screen.  It uses ARGB format for the color representation.
*/
class Color {
public:
	Color(void);

	/*
	* Initializes the individual colors by converting the given floats in the interval [0, 1] to 8-bit unsigned integer representations.
	*/
	Color(float r, float g, float b, float a);

	/*
	* Initializes the individual colors with the given integers in the interval [0,255].
	*/
	Color(uint8_t r, uint8_t g, uint8_t b, uint8_t a);

	/*
	* Converts the individual color components of this Color into a 32-bit integer representation, where the format 
	* is ARGB.
	*/
	uint32_t toInt32();

private:
	uint8_t m_R, m_G, m_B, m_A;
};

#endif Color_H