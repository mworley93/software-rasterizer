#include <glm\glm.hpp>

/* 
* This structure represents a simple line.
*/
struct Line {

	void setPositions(glm::vec4 a, glm::vec4 b) {
		p0 = a;
		p1 = b;
	}

	void setColors(glm::vec4 a, glm::vec4 b) {
		c0 = a;
		c1 = b;
	}

	void setNormals(glm::vec4 a, glm::vec4 b) {
		n0 = a;
		n1 = b;
	}

	// Positions
	glm::vec4 p0;
	glm::vec4 p1;

	// Colors
	glm::vec4 c0;
	glm::vec4 c1;

	// Normals
	glm::vec4 n0;
	glm::vec4 n1;
};