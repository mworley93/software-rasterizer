#pragma once

/*
* This header contains enumerations used to change the settings of the renderer.  
*/

enum RenderMode {
	POINTS,
	LINES,
	LINE_STRIP,
	TRIANGLES,
};

enum DepthFunction {
	ALWAYS,
	NEVER,
	LESS,
	GREATER,
	LEQUAL,
	GEQUAL,
	EQUAL,
	NOT_EQUAL,
};

enum WindingOrder {
	CLOCKWISE,
	COUNTER_CLOCKWISE,
};

enum Face {
	BACK,
	FRONT, 
	FRONT_AND_BACK,
};

enum BufferType {
	ARRAY_BUFFER,
	ELEMENT_BUFFER,
};

enum VertexAttribute {
	POSITIONS,
	COLORS,
	NORMALS,
};