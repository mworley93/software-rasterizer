# Software Rasterizer #

## Description ##
In essence, this project is a small graphics demo made for educational purposes.  I decided to implement it after becoming familiar with OpenGL 3.3 basics and the graphics pipeline.  I was fuzzy on the details on how exactly a 3D dimensional model became a flat, 2D image displayed on your monitor.  What better way to learn than to code up a basic rasterizer?  Because of my OpenGL experience, I modeled some of the methods after OpenGL's style and added a simple .obj loader for testing. 

## Implementation ##
As per the graphics pipeline, models that are loaded in undergo a model-view-projection transformation.  Triangle clipping is done using a modified version of the Sutherland-Hodgeman clipping algorithm. Triangles are rasterized using a barycentric coordinate algorithm, which is a popular one used on modern GPUs due to its ability to be easily parallelized.  Colors and normals are also interpolated using this approach.  Shaders are not incredibly customizable, but basic programmable vertex and fragment shaders can be added.

## Installation ##
A Windows exe is provided in a zip for download in the Downloads tab of this repository.

## Example Output ## 
Suzanne, Blender's test model, was loaded and rendered on an Intel i7 at 2.00 GHz with 8 GB of RAM.  This version of Suzanne consists of 967 faces.

The rasterizer renders at roughly 125 FPS at the default view shown here.

![suzanne.PNG](https://bitbucket.org/repo/GaM5gb/images/2231116347-suzanne.PNG)

Rendering Suzanne up close drops the frame rate to 36 FPS.

![suzanne2.PNG](https://bitbucket.org/repo/GaM5gb/images/3437796019-suzanne2.PNG)